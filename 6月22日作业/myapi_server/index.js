const express = require('express')
const app = express()
const bodyParser = require('body-parser')
// 处理静态资源
app.use(express.static('public'))
// 处理参数
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// 设置允许跨域访问该服务
app.all('*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.setHeader('Content-Type', 'application/json;charset=utf-8')
  res.header('Access-Control-Allow-Headers', 'Content-Type,Content-Length, Authorization, Accept,X-Requested-With, mytoken')
  res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS')
  res.header('X-Powered-By', ' 3.2.1')
  if (req.method == 'OPTIONS') res.sendStatus(200)
  /*让options请求快速返回*/ else next()
});
// 测试接口1
app.get('/getdata1', (req, res) => {
  setTimeout(function(){
    res.send('{"success":true,"message":"getdata1获取数据成功"}')
  },2000)
})
// 测试接口1
app.get('/getdata2', (req, res) => {
  res.send('{"success":true,"message":"getdata2获取数据成功"}')
})

// 登录的接口地址   /login  需要携带参数 uname 和 pwd
app.post('/login', (req, res) => {
  if(req.body.uname == 'zhangsan' && req.body.pwd == '123456') {
    res.send('{"success":true,"message":"登录成功","uid":"100989"}')
  } else {
    res.send('{"success":false,"message":"登录失败"}')
  }
})
// 查询某个用户的订单列表  /orderlist  需要携带参数  uid:某个用户的唯一标识
app.get('/orderlist', (req, res) => {
  if(!req.query.uid) {
    res.send('{"success":false,"message":"参数不全,需要携带uid"}')
  }else{
    if(req.query.uid == '100989') {
      res.send('{"success":true,"message":"订单列表获取成功", "list":[{"orderid":"200901019298", "title":"订单标题1"},{"orderid":"200232131231", "title":"订单标题2"}]}')
    } else {
      res.send('{"success":false,"message":"uid不正确"}')
    }
  }
})


app.get('/a1', (req, res) => {
  res.send('{"success":true,"message":"a1获取数据成功"}')
})
app.get('/a2', (req, res) => {
  res.send('{"success":true,"message":"a2获取数据成功"}')
})
app.get('/a3', (req, res) => {
  res.send('{"success":true,"message":"a3获取数据成功"}')
})

app.get('/getnewslist', (req, res) => {
  res.send(`
  {"success":true,"message":"获取新闻列表成功", "newslist": [{
    "id":1001,
    "title":"新闻标题1",
    "content":"新闻内容1"
  },{
    "id":1002,
    "title":"新闻标题2",
    "content":"新闻内容2"
  },{
    "id":1003,
    "title":"新闻标题3",
    "content":"新闻内容3"
  }]}
  `)
})

app.get('/getnewsdetail', (req, res) => {
  if(!req.query.newsid) {
    res.send('{"success":false,"message":"newsid必须传递"}')
    return
  }
  res.send(`
  {
    "success":true,
    "message":"获取新闻列表成功", 
    "news": {
      "id":100${req.query.newsid},
      "title":"新闻标题${req.query.newsid}",
      "content":"新闻内容${req.query.newsid}"
    }
  }
  `)
})

app.post('/addnews', (req, res) => {
  var title = req.body.title
  var content = req.body.content
  if(!title || !content) {
    res.send('{"success":false,"message":"title和content必须传递"}')
    return
  }
  res.send(`
  {
    "success":true,
    "message":"增加新闻成功", 
    "news": {
      "title":"新闻标题${title}",
      "content":"新闻内容${content}"
    }
  }
  `)
})

app.put('/modify/:id', (req, res) => {
  if (!req.params.id) {
    res.send('{"success":false,"message":"id必须传递"}')
    return
  }
  res.send(`
  {
    "success":true,
    "message":"修改id为${req.params.id}的新闻成功"
  }
  `)
})
app.delete('/delete/:id', (req, res) => {
  if (!req.params.id) {
    res.send('{"success":false,"message":"id必须传递"}')
    return
  }
  res.send(`
  {
    "success":true,
    "message":"删除id为${req.params.id}的新闻成功"
  }
  `)
})

// 启动监听
app.listen(3001, () => {
  console.log('running...')
})